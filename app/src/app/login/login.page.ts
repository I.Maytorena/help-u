import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { User } from "../shared/user.class";
import { AuthService } from "../services/auth.service";
import { Storage } from '@ionic/storage';
import { ActionSheetController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user:User = new User();

  constructor(private actionSheetController: ActionSheetController, private router:Router, private authSvc: AuthService, private storage: Storage) {}

  ngOnInit() {
  }

  async onLogin() {
    const user = await this.authSvc.onLogin(this.user);
    if (user) {
      this.storage.set('onLogin', true);
      this.router.navigateByUrl('/tabs/home')
    }
    else {
      console.log("Usuario Incorrecto")
    }

  }
  async callActionShare() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Iniciar sesion con',
      buttons: [{
        text: 'Google',
        icon: 'logo-google',
      }, {
        text: 'Facebook',
        icon: 'logo-facebook',
      }, {
        text: 'GitHub',
        icon: 'logo-github',
      }, {
        text: 'Cancelar',
        role: 'cancel',
      }]
    });
    await actionSheet.present();
  }
}
