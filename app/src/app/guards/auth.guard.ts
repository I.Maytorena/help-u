import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from "../services/auth.service";
import { Router } from "@angular/router";
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router:Router,private storage:Storage){}
  async canActivate() {
    const isIntroShowed = await this.storage.get("isIntroShowed");
    const onLogin = await this.storage.get("onLogin");
    if (isIntroShowed) {
      if ( this.authService.isLogged || onLogin ) {
        return true;
      } else {
        this.router.navigateByUrl('/login');
        return false;
      }
    } else {
      this.router.navigateByUrl('/intro');
      return false;
    }
  }
  
}
