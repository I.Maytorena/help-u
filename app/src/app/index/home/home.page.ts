import { Component, OnInit } from '@angular/core';
import { FirebaseApp, FirebaseDatabase, FirebaseFirestore } from '@angular/fire';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  //  Should be one of those classes
  card = {
    title:'',
    subtitle:'',
    image:''
  }
  constructor(){}
  // constructor(conn:FirebaseDatabase, store: FirebaseFirestore) { }
  // getInfoForToday(){
  //   this.card.image = this.store.plan(randomPlan.image)
  //   this.card.title = this.store.plan(randomPlan.title)
  //   this.card.subtitle = this.store.plan(randomPlan.information)
  // }
  ngOnInit() {
  }

}
