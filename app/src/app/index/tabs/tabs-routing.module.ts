import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page'; 

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: '../home/home.module#HomePageModule',
      },
      {
        path: 'plans',
        loadChildren: '../plans/plans.module#PlansPageModule',
      },
      {
        path: 'statistics',
        loadChildren: '../statistics/statistics.module#StatisticsPageModule',
      },
      {
        path: 'board',
        loadChildren: '../board/board.module#BoardPageModule',
      },
      {
        path: 'recipes',
        loadChildren: '../recipes/recipes.module#RecipesPageModule',
      },
      {
        path: 'settings',
        loadChildren: '../settings/settings.module#SettingsPageModule',
      },
    ]
  },
  {
    path:'',
    redirectTo:'/tabs/home',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
