import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})

export class MenuPage implements OnInit {

  pages = [
    {
      title:'Inicio',
      url:'/tabs/home'
    }
  ]

  selectedPath = '' ;
  constructor(private router:Router, private menu: MenuController) {
    this.router.events.subscribe((event:RouterEvent)=>{
      if ( event && event.url ) {
        this.selectedPath = event.url;
      }
    })
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }

  ngOnInit(){}
}


