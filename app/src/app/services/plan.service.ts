import { Injectable } from '@angular/core';
import { AngularFirestore, CollectionReference } from '@angular/fire/firestore';

export interface plan {
  name:string,
  food:CollectionReference[],
}

export interface food {
  name:string,
  information:string,
  image:string,
}

export interface exercise {
  name:string,
  information:string,
  image:string,
}

export interface user {
  name:string,
  information:string,
  image:string,
  plan:CollectionReference[],
}

@Injectable({
  providedIn: 'root'
})
export class PlanService {
  constructor(db: AngularFirestore) { }
}