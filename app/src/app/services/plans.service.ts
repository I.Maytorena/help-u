import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, CollectionReference } from '@angular/fire/firestore';

export interface Plan {
  name:string,
  food:CollectionReference[],
}

@Injectable({
  providedIn: 'root'
})
export class PlansService {
  private plan : AngularFirestoreCollection<PlansService>
  constructor(db: AngularFirestore  ) { }
}
