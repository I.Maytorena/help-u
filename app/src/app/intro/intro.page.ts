import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from "@ionic/storage";
import { ToastController, LoadingController } from '@ionic/angular';
import { NgModel } from '@angular/forms';


@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {
  slidesOptions = {
    initialSlide: 0,
    direction: 'horizontal',
    speed: 400,
    centeredSlides: true,
    spaceBetween: 8,
    slidesPerView: 1.5,
    freeMode: true,
    loop: true
  };

  slides = [
    {
      id:'Usuario',
      input:true,
      title:'Bienvenido',
      subtitle:'Antes de comenzar',
      icon:'contact',
      content:'Ingresa tu nombre',
      genere:true,
      top_icon:'close'
    },
    {
      icon:'list-box',
      title:'Aqui Vamos!',
      subtitle:'¿Que tan preparado te sientes?',
      content:'Elige un Plan',
      plan:true,
      top_icon:'close'
    },
    {
      finish:true,
      title:'Help-U',
      subtitle:'¿Estás listo?',
    }
  ]
  constructor(private router:Router, private storage:Storage, public toastController: ToastController, private loadingController: LoadingController) { }
  ngOnInit() {
  }
  skip(){
    this.storage.set('isIntroShowed', true);
    this.router.navigateByUrl('/login');
  }
  async segmentChanged(this) {
    let levelSelected = 'Nivel: Dificil';
    let hard = true;
    if (this.level =='easy') {
      levelSelected = 'Nivel: Facil'
      hard = false
    }
    const toast = await this.toastController.create({
      color:'primary',
      position: 'top',
      message: levelSelected,
      duration: 1000,
      buttons: [
        {
          icon: (hard) ? 'fitness' : 'pizza',
        }
      ]
    });
    toast.present();
  }

  async loadder(){
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      message: 'Guardando tus preferencias',
      duration: 200
    });
    await loading.present();
    // const { role, data } = await loading.onDidDismiss();
    this.storage.set('isIntroShowed', true);
    // const onSession = this.storage.get('onLogin');
    this.router.navigateByUrl('/login');

    // console.log('Loading dismissed!');
  }
}